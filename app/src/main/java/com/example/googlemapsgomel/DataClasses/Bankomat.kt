package com.example.googlemapsgomel.DataClasses

import android.os.Parcel
import android.os.Parcelable

data  class Bankomat(
                      var gps_x: Double? = null,
                      var gps_y: Double? = null
                      ) : GpsX_GpsY
{

    constructor(parcel: Parcel) : this() {

        gps_x = parcel.readValue(Double::class.java.classLoader) as? Double
        gps_y = parcel.readValue(Double::class.java.classLoader) as? Double

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeValue(gps_x)
        parcel.writeValue(gps_y)

    }

    override fun getGpsX(): Double {
        return gps_x!!
    }

    override fun getNameClass(): String {
        return "BANKOMAT"
    }

    override fun getGpsY(): Double {
        return gps_y!!
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bankomat> {
        override fun createFromParcel(parcel: Parcel): Bankomat {
            return Bankomat(parcel)
        }

        override fun newArray(size: Int): Array<Bankomat?> {
            return arrayOfNulls(size)
        }
    }

}



