package com.example.googlemapsgomel.DataClasses

import android.os.Parcel
import android.os.Parcelable

data class Filials(

    var filial_name : String? = null,
    var GPS_X  : Double? = null,
    var GPS_Y  : Double? = null

) : GpsX_GpsY {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double

    ) {
    }

    override fun getGpsX(): Double {
       return GPS_X!!
    }

    override fun getNameClass(): String {
        return filial_name!!
    }

    override fun getGpsY(): Double {
        return GPS_Y!!
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<Filials> {
        override fun createFromParcel(parcel: Parcel): Filials {
            return Filials(parcel)
        }

        override fun newArray(size: Int): Array<Filials?> {
            return arrayOfNulls(size)
        }
    }

}
