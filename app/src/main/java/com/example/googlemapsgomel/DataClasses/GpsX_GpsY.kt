package com.example.googlemapsgomel.DataClasses

import android.os.Parcelable

interface GpsX_GpsY : Parcelable {

        fun getGpsX() : Double
        fun getNameClass(): String
        fun getGpsY(): Double

}