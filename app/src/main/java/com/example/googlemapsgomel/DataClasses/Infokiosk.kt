package com.example.googlemapsgomel.DataClasses

import android.os.Parcel
import android.os.Parcelable

data class Infokiosk (

    var gps_x: Double? = null,
    var gps_y: Double? = null


) : GpsX_GpsY {
    constructor(parcel: Parcel) : this(

        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double

    ) {
    }

    override fun getGpsX(): Double {
       return gps_x!!
    }

    override fun getNameClass(): String {
        return "INFOBOX"
    }

    override fun getGpsY(): Double {
        return gps_y!!
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<Infokiosk> {
        override fun createFromParcel(parcel: Parcel): Infokiosk {
            return Infokiosk(parcel)
        }

        override fun newArray(size: Int): Array<Infokiosk?> {
            return arrayOfNulls(size)
        }
    }

}