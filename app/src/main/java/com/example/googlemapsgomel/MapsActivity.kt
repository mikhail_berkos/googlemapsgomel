package com.example.googlemapsgomel


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.googlemapsgomel.DataClasses.GpsX_GpsY
import com.example.googlemapsgomel.Retrofit.Common
import com.example.googlemapsgomel.Retrofit.RetrofitServices


import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.googlemapsgomel.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.maps.android.SphericalUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single


import io.reactivex.rxjava3.schedulers.Schedulers


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mService: RetrofitServices

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)




        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)



    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        addMarkers(mMap)
        val gomel = LatLng(52.4345, 30.9754)
        val position = CameraPosition.Builder().target(gomel).zoom(11.0f).build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position))

    }

    private fun addMarkers(mMap : GoogleMap){
        val dotOnMap = LatLng(52.425163, 31.015039)
        mMap.addMarker(
            MarkerOptions()
                .position(dotOnMap)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .title("Точка удаления"))
        mService = Common.retrofitService
        Single.zip(
            mService.getDotOnMapList(),
            mService.getFilOnMapList(),
            mService.getInfOnMapList(),
            {
                a,b,c -> val list : MutableList<GpsX_GpsY> = mutableListOf()
                list.addAll(a)
                list.addAll(b)
                list.addAll(c)
                return@zip list

            })
            .map{ return@map it.sortedWith( compareBy{SphericalUtil.computeDistanceBetween(dotOnMap, LatLng(it.getGpsX(),it.getGpsY()))})}
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { it -> for(i in 0..10){

                   mMap.addMarker(
                       MarkerOptions()
                           .position(LatLng(it[i].getGpsX(), it[i].getGpsY()))
                           .title(it[i].getNameClass()))}



                       },{})
    }

}