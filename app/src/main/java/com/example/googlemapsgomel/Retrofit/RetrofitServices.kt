package com.example.googlemapsgomel.Retrofit

import com.example.googlemapsgomel.DataClasses.Bankomat
import com.example.googlemapsgomel.DataClasses.Filials
import com.example.googlemapsgomel.DataClasses.Infokiosk
import io.reactivex.rxjava3.core.Single
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitServices {
    @GET("atm?city=Гомель")
    fun getDotOnMapList(): Single<MutableList<Bankomat>>

    @GET("infobox?city=Гомель")
    fun getInfOnMapList(): Single<MutableList<Infokiosk>>

    @GET("filials_info?city=Гомель")
    fun getFilOnMapList(): Single<MutableList<Filials>>




}