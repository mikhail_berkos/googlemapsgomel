package com.example.googlemapsgomel.Retrofit

object Common {
    private const val BASE_URL = "https://belarusbank.by/api/"
    val retrofitService: RetrofitServices
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitServices::class.java)
}